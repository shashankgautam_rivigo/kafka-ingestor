package com.rivigo.kafka.ingestor.app.service;

import com.rivigo.kafka.ingestor.common.Event;

import java.util.List;

/**
 * @author shashankgautam
 */
public interface IIngestorService {

    void ingest(final Event event);

    void ingestBulk(final List<Event> event);
}
