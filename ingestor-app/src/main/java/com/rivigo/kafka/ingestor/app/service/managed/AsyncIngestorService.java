package com.rivigo.kafka.ingestor.app.service.managed;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rivigo.kafka.ingestor.app.IngestorConfig;
import com.rivigo.kafka.ingestor.app.service.impl.IngestorService;
import com.rivigo.kafka.ingestor.app.service.impl.SidelineIngestorService;
import io.dropwizard.lifecycle.Managed;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author shashankgautam
 */
@Slf4j
@AllArgsConstructor
public class AsyncIngestorService implements Managed {

    private static final long DEFAULT_INITIAL_DELAY_IN_SEC = 1L; //TODO make it configurable
    private static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.SECONDS; //TODO make it configurable

    @Getter
    private final ScheduledExecutorService mainQueueExecutor;

    @Getter
    private final ScheduledExecutorService sidelineQueueExecutor;
    private final ObjectMapper mapper;
    private final KafkaPublisherService publisher;
    private final int batchSize;
    private final IngestorConfig config;

    @Override
    public void start() throws Exception {
        mainQueueExecutor.scheduleAtFixedRate(new IngestorService(mapper, publisher, batchSize),
                DEFAULT_INITIAL_DELAY_IN_SEC, config.getMainQueueScheduledIntervalInSecond(), DEFAULT_TIME_UNIT);
        sidelineQueueExecutor.scheduleAtFixedRate(new SidelineIngestorService(mapper, publisher, batchSize),
                DEFAULT_INITIAL_DELAY_IN_SEC, config.getSidelineQueueScheduledIntervalInSecond(), DEFAULT_TIME_UNIT);
        log.info("Started mainQueueExecutor and sidelineQueueExecutor...");
    }

    @Override
    public void stop() throws Exception {
        mainQueueExecutor.shutdown();
        sidelineQueueExecutor.shutdown();
        log.info("Stopped mainQueueExecutor and sidelineQueueExecutor...");
    }
}
