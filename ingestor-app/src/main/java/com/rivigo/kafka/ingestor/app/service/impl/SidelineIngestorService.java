package com.rivigo.kafka.ingestor.app.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rivigo.kafka.ingestor.app.service.managed.KafkaPublisherService;
import com.rivigo.kafka.ingestor.app.service.managed.DiskQueueService;
import com.rivigo.kafka.ingestor.common.Event;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author shashankgautam
 */
@Slf4j
@AllArgsConstructor
public class SidelineIngestorService implements Runnable {

    private final ObjectMapper mapper;
    private final KafkaPublisherService factory;
    private int maxBatchSize;

    @Override
    public void run() {

        final List<Event> eventList = Collections.emptyList();
        final long queueSize = DiskQueueService.getSidelineQueue().size();

        if (queueSize == 0) {
            log.info("[Sideline] Queue is empty...Nothing to do!");
            return;
        }

        try {
            log.info("[Sideline] Queue Size - {}", queueSize);
            /*while (DiskQueueService.getSidelineQueue().size() == 0 || eventList.size() != maxBatchSize) {*/
            eventList.add(mapper.readValue(DiskQueueService.getSidelineQueue().dequeue(), Event.class));
        } catch (final IOException e) {
            log.error("[Sideline] Exception occurred while creating list for sidelining...", e);
            throw new RuntimeException(e);
        }
        publish(eventList);
    }

    private void publish(final List<Event> events) {
        events.forEach(event -> {
            try {
                log.info("[Sideline] Trying to publish to kafka...");
                factory.getPublisher().send(new ProducerRecord<>(event.getMetadata().getTenant(), event));
                log.info("[Sideline] Published to kafka successfully...");
            } catch (final Exception e) {
                log.error("[Sideline - Ignoring event] Exception occurred while publishing to sideline kafka...", e);
            }
        });
    }
}
