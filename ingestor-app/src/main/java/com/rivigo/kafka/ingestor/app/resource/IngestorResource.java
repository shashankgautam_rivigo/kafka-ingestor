package com.rivigo.kafka.ingestor.app.resource;

import com.codahale.metrics.annotation.Timed;
import com.rivigo.kafka.ingestor.app.service.IIngestorService;
import com.rivigo.kafka.ingestor.common.Event;
import io.dropwizard.hibernate.UnitOfWork;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author shashankgautam
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("/v1/ingestor")
@Slf4j
@AllArgsConstructor
public class IngestorResource {

    private final IIngestorService service;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Timed
    @UnitOfWork
    @Path("/ingest")
    public Response ingest(@NotNull final Event event) {
        service.ingest(event);
        return Response.status(Response.Status.CREATED).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Timed
    @UnitOfWork
    @Path("/ingest/bulk")
    public Response ingestBulk(@NotNull final List<Event> events) {
        service.ingestBulk(events);
        return Response.status(Response.Status.CREATED).build();
    }
}
