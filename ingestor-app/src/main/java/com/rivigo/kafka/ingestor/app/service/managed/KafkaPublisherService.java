package com.rivigo.kafka.ingestor.app.service.managed;

import com.rivigo.kafka.ingestor.app.kafka.KafkaPoolFactory;
import com.rivigo.kafka.ingestor.app.kafka.KafkaPublisher;
import com.rivigo.kafka.ingestor.common.Event;
import io.dropwizard.lifecycle.Managed;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.kafka.clients.producer.KafkaProducer;

import java.util.Properties;

/**
 * @author shashankgautam
 */
@Slf4j
public class KafkaPublisherService implements Managed {

    private final Properties properties;
    private final int poolSize;

    @Getter
    private KafkaPublisher publisher;

    @Getter
    private GenericObjectPool<KafkaProducer<String, Event>> pool;

    /**
     * Constructor
     *
     * @param properties :: to set
     * @param poolSize   :: to set
     */
    public KafkaPublisherService(final Properties properties, final int poolSize) {
        this.properties = properties;
        this.poolSize = poolSize;
    }

    @Override
    public void start() throws Exception {
        this.pool = initPool(properties, poolSize);
        this.publisher = new KafkaPublisher(pool);
        log.info("Started kafka producer pool...");
    }

    @Override
    public void stop() {
        this.pool.close();
        log.info("Stopped kafka producer pool...");
    }

    private GenericObjectPool<KafkaProducer<String, Event>> initPool(final Properties properties, final int poolSize) {
        final GenericObjectPool<KafkaProducer<String, Event>> pool = new GenericObjectPool<>(new KafkaPoolFactory(properties));
        pool.setMinIdle(poolSize);
        pool.setMaxIdle(poolSize);
        pool.setMaxTotal(poolSize);
        return pool;
    }
}