package com.rivigo.kafka.ingestor.app.service.managed;

import com.leansoft.bigqueue.BigQueueImpl;
import com.leansoft.bigqueue.IBigQueue;
import com.rivigo.kafka.ingestor.app.IngestorConfig;
import io.dropwizard.lifecycle.Managed;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shashankgautam
 */
@Slf4j
public class DiskQueueService implements Managed {

    @Getter
    private static IBigQueue mainQueue;

    @Getter
    private static IBigQueue sidelineQueue;

    private IngestorConfig config;

    /**
     * Init
     *
     * @param config ::
     */
    public DiskQueueService(final IngestorConfig config) {
        this.config = config;
    }

    @Override
    public void start() throws Exception {
        try {
            mainQueue = new BigQueueImpl(config.getBigQueueDirectory(), config.getMainQueueName());
            sidelineQueue = new BigQueueImpl(config.getBigQueueDirectory(),
                    String.format("%s%s", config.getMainQueueName(), config.getSidelineQueueName()));

            log.info("Initialized main queue with dir - {}, queueName - {}", config.getBigQueueDirectory(), config.getMainQueueName());
            log.info("Initialized sideline queue with dir - {}, queueName - {}", config.getSidelineQueueName(), config.getSidelineQueueName());
        } catch (final Exception e) {
            log.error("Exception occurred while creating big queue...", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stop() throws Exception {
        mainQueue.close();
        sidelineQueue.close();
        log.info("Closed main and sideline queues...");
    }

}
