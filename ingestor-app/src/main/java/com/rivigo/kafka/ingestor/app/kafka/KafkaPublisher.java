package com.rivigo.kafka.ingestor.app.kafka;

import com.rivigo.kafka.ingestor.common.Event;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.List;

/**
 * @author shashankgautam
 */
@Slf4j
@AllArgsConstructor
public class KafkaPublisher {

    private final GenericObjectPool<KafkaProducer<String, Event>> pool;

    /**
     * @param message :: to send
     * @throws Exception ::
     */
    public void send(final ProducerRecord<String, Event> message) throws Exception {
        KafkaProducer<String, Event> producer = null;
        try {
            producer = pool.borrowObject();
            producer.send(message);
        } finally {
            if (producer != null) {
                pool.returnObject(producer);
            }
        }
    }

    /**
     * @param messages ::
     * @throws Exception ::
     */
    public void send(final List<ProducerRecord<String, Event>> messages) throws Exception {
        messages.forEach(message -> {
            try {
                send(message);
            } catch (final Exception e) {
                log.error("Exception - ", e);
                throw new RuntimeException(e);
            }
        });
    }
}
