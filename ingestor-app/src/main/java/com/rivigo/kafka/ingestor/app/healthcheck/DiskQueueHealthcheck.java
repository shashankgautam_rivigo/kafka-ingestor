package com.rivigo.kafka.ingestor.app.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import com.rivigo.kafka.ingestor.app.service.managed.DiskQueueService;

/**
 * @author shashankgautam
 */
public class DiskQueueHealthcheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {
        return Result.healthy(String.format("MainQueueSize=%s, SidelineQueueSize=%s",
                DiskQueueService.getMainQueue().size(), DiskQueueService.getSidelineQueue().size()));
    }
}
