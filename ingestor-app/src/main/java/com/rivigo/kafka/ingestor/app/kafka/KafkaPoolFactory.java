package com.rivigo.kafka.ingestor.app.kafka;

import com.rivigo.kafka.ingestor.common.Event;
import lombok.AllArgsConstructor;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.kafka.clients.producer.KafkaProducer;

import java.util.Properties;

/**
 * @author shashankgautam
 */
@AllArgsConstructor
public class KafkaPoolFactory extends BasePooledObjectFactory<KafkaProducer<String, Event>> {

    private final Properties properties;

    @Override
    public KafkaProducer<String, Event> create() throws Exception {
        return new KafkaProducer<>(properties, null, new EventSerializer());
    }

    @Override
    public PooledObject<KafkaProducer<String, Event>> wrap(final KafkaProducer<String, Event> producer) {
        return new DefaultPooledObject<>(producer);
    }
}