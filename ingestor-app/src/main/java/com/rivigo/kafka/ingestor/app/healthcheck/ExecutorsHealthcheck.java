package com.rivigo.kafka.ingestor.app.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.ScheduledExecutorService;

/**
 * @author shashankgautam
 */
@AllArgsConstructor
public class ExecutorsHealthcheck extends HealthCheck {

    @Getter
    private final ScheduledExecutorService mainQueueExecutor;

    @Getter
    private final ScheduledExecutorService sidelineQueueExecutor;

    private enum Status {
        Running, SHUTDOWN, TERMINATED
    }

    @Override
    protected Result check() throws Exception {
        Status mainQueueStatus = Status.Running;
        Status sidelineQueueStatus = Status.Running;

        if (mainQueueExecutor.isShutdown()) {
            mainQueueStatus = Status.SHUTDOWN;
        } else if (mainQueueExecutor.isTerminated()) {
            mainQueueStatus = Status.TERMINATED;
        }

        if (sidelineQueueExecutor.isShutdown()) {
            sidelineQueueStatus = Status.SHUTDOWN;
        } else if (sidelineQueueExecutor.isTerminated()) {
            sidelineQueueStatus = Status.TERMINATED;
        }

        return sidelineQueueStatus.equals(Status.Running) && mainQueueStatus.equals(Status.Running) ?
                Result.healthy(String.format("mainQueueExecutorStatus: %s, sidelineQueueExecutorStatus: %s",
                        mainQueueStatus, sidelineQueueStatus)) :
                Result.unhealthy(String.format("mainQueueExecutorStatus: %s, sidelineQueueExecutorStatus: %s",
                        mainQueueStatus, sidelineQueueStatus));
    }
}
