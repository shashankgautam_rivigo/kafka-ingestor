package com.rivigo.kafka.ingestor.app.resource;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author shashankgautam
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("/v1/disks/queues")
@Slf4j
@AllArgsConstructor
public class DiskQueueResource {
}
