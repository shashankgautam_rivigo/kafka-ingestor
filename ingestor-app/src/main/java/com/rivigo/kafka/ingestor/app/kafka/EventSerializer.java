package com.rivigo.kafka.ingestor.app.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rivigo.kafka.ingestor.common.Event;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

/**
 * @author shashankgautam
 */
@Slf4j
public class EventSerializer implements Serializer<Event> {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void configure(final Map<String, ?> map, final boolean bool) {
    }

    @Override
    public byte[] serialize(final String str, final Event event) {
        byte[] retVal;
        try {
            retVal = objectMapper.writeValueAsString(event).getBytes();
        } catch (final Exception e) {
            log.error("Exception - ", e);
            throw new RuntimeException(e);
        }
        return retVal;
    }

    @Override
    public void close() {
    }
}
