package com.rivigo.kafka.ingestor.app;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rivigo.kafka.ingestor.app.healthcheck.DiskQueueHealthcheck;
import com.rivigo.kafka.ingestor.app.healthcheck.ExecutorsHealthcheck;
import com.rivigo.kafka.ingestor.app.resource.IngestorResource;
import com.rivigo.kafka.ingestor.app.service.impl.IngestorService;
import com.rivigo.kafka.ingestor.app.service.managed.AsyncIngestorService;
import com.rivigo.kafka.ingestor.app.service.managed.DiskQueueService;
import com.rivigo.kafka.ingestor.app.service.managed.KafkaPublisherService;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import lombok.extern.slf4j.Slf4j;

import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author shashankgautam
 */
@Slf4j
public class IngestorApp extends Application<IngestorConfig> {

    private static final String APP_NAME = "Kafka-ingestor";

    /**
     * @param args to start java process
     * @throws Exception ::
     */
    public static void main(final String[] args) throws Exception {
        final IngestorApp app = new IngestorApp();
        log.info("Starting app - {}", APP_NAME);
        app.run(args);
    }

    @Override
    public void initialize(final Bootstrap<IngestorConfig> bootstrap) {
        super.initialize(bootstrap);
    }

    @Override
    public void run(final IngestorConfig config, final Environment environment) throws Exception {
        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);

        final DiskQueueService managedQueue = new DiskQueueService(config);
        final ScheduledExecutorService mainPublisherExecutor =
                environment.lifecycle().scheduledExecutorService("main-publisher").build();
        final ScheduledExecutorService sidelinePublisherExecutor =
                environment.lifecycle().scheduledExecutorService("sideline-publisher").build();
        final KafkaPublisherService kafkaPublisherService =
                new KafkaPublisherService(createKafkaProperties(config), config.getKafkaPoolSize());
        final AsyncIngestorService asyncIngestorService = new AsyncIngestorService(
                mainPublisherExecutor, sidelinePublisherExecutor, environment.getObjectMapper(),
                kafkaPublisherService, config.getKafkaBatchSize(), config);
        final IngestorResource ingestorResource = new IngestorResource(
                new IngestorService(environment.getObjectMapper(), kafkaPublisherService, config.getKafkaBatchSize()));
        final ExecutorsHealthcheck executorsHealthcheck =
                new ExecutorsHealthcheck(mainPublisherExecutor, sidelinePublisherExecutor);
        final DiskQueueHealthcheck diskQueueHealthcheck = new DiskQueueHealthcheck();

        /*
         * Register to jersey
         */
        environment.lifecycle().manage(managedQueue);
        environment.lifecycle().manage(kafkaPublisherService);
        environment.lifecycle().manage(asyncIngestorService);
        environment.healthChecks().register("disk-queue", diskQueueHealthcheck);
        environment.healthChecks().register("async-executor", executorsHealthcheck);
        environment.jersey().register(ingestorResource);
    }

    /*
     * creation of kafka properties
     */
    private Properties createKafkaProperties(final IngestorConfig config) {
        final Properties properties = new Properties();
        properties.put("bootstrap.servers", config.getKafkaUrl() + ":" + config.getKafkaPort());
        properties.put("client.id", APP_NAME);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return properties;
    }
}