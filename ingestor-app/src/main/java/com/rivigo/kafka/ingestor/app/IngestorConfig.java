package com.rivigo.kafka.ingestor.app;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.dropwizard.Configuration;
import lombok.Getter;

/**
 * @author shashankgautam
 */
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IngestorConfig extends Configuration {

    private String bigQueueDirectory = "/tmp";

    private String mainQueueName = "kafka-ingestor-test";

    private String sidelineQueueName = "kafka-ingestor-test-sideline";

    private String kafkaUrl = "localhost";

    private int kafkaPort = 3306;

    private int kafkaBatchSize = 100;
    private long mainQueueScheduledIntervalInSecond = 10;
    private long sidelineQueueScheduledIntervalInSecond = 10;
    private int kafkaPoolSize = 10;
}
