package com.rivigo.kafka.ingestor.app.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.rivigo.kafka.ingestor.app.service.IIngestorService;
import com.rivigo.kafka.ingestor.app.service.managed.DiskQueueService;
import com.rivigo.kafka.ingestor.app.service.managed.KafkaPublisherService;
import com.rivigo.kafka.ingestor.common.Event;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.IOException;
import java.util.List;

/**
 * @author shashankgautam
 */
@Slf4j
@AllArgsConstructor
public class IngestorService implements IIngestorService, Runnable {

    private final ObjectMapper mapper;
    private final KafkaPublisherService factory;
    private int batchSizeToProduce;

    @Override
    public void ingest(final Event event) {
        ingestBulk(Lists.newArrayList(event));
    }

    @Override
    public void ingestBulk(final List<Event> events) {
        events.forEach(event -> {
            try {
                DiskQueueService.getMainQueue().enqueue(mapper.writeValueAsBytes(event));
                log.info("Size - {} ", DiskQueueService.getMainQueue().size());
            } catch (final IOException e) {
                log.error("Exception occurred...", e);
                throw new RuntimeException(e);
            }
            log.info("Ingested event - {}", event);
        });
    }

    @Override
    public void run() {
        final ImmutableList.Builder<Event> eventListBuilder = ImmutableList.builder();
        final long queueSize = DiskQueueService.getMainQueue().size();

        if (queueSize == 0) {
            log.info("[Main] Queue Empty...Nothing to do!");
        } else if (queueSize >= batchSizeToProduce) {
            try {
                for (int count = 0; count < batchSizeToProduce; count++) {
                    eventListBuilder.add(mapper.readValue(DiskQueueService.getMainQueue().dequeue(), Event.class));
                }
            } catch (final IOException e) {
                log.error("[Main] Exception occurred while ingesting...", e);
                throw new RuntimeException(e);
            }
            publish(eventListBuilder.build());
        } else {
            log.info("[Main] Size - {}, Waiting for the queue size to be equal to batchSize {}.",
                    DiskQueueService.getMainQueue().size(), batchSizeToProduce);
        }
    }

    private void publish(final List<Event> events) {
        log.info("No of events - {}", events.size());
        events.forEach(event -> {
            try {
                log.info("[Main] Trying to publish to kafka...");
                factory.getPublisher().send(new ProducerRecord<>(event.getMetadata().getTenant(), event));
                log.info("[Main] Published to kafka successfully...");
            } catch (final Exception e) {
                log.error("[Main] Exception occurred while publishing to kafka...hence sidelining", e);
                try {
                    DiskQueueService.getSidelineQueue().enqueue(mapper.writeValueAsBytes(event));
                } catch (final IOException e1) {
                    log.error("[Ignoring event] Exception occurred while sidelining to disk queue...", e);
                }
            }
        });
    }
}
