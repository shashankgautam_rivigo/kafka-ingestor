package com.rivigo.kakfa.ingestor.client.feign;

import com.rivigo.kafka.ingestor.common.Event;
import feign.Response;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author shashankgautam
 */
public interface IngestorFeignClient {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/v1/ingestor/ingest")
    Response ingest(final Event event);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/v1/ingestor/ingest/bulk")
    Response ingestBulk(final List<Event> events);
}
