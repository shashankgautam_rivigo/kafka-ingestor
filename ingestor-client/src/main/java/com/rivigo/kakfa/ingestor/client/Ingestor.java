package com.rivigo.kakfa.ingestor.client;

import com.rivigo.kafka.ingestor.common.Event;
import com.rivigo.kakfa.ingestor.client.feign.IngestorFeignClient;
import feign.Response;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * @author shashankgautam
 */
@AllArgsConstructor
public class Ingestor {

    private final IngestorFeignClient client;

    /**
     * @param event :: to ingest
     * @return ::
     */
    public Response ingest(final Event event) {
        return this.client.ingest(event);
    }

    /**
     * @param events :: to ingest
     * @return ::
     */
    public Response ingestBulk(final List<Event> events) {
        return this.client.ingestBulk(events);
    }
}
