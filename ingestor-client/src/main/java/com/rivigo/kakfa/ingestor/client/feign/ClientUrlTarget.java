package com.rivigo.kakfa.ingestor.client.feign;

import feign.Target;

/**
 * @author shashankgautam
 */
public class ClientUrlTarget<T> extends Target.HardCodedTarget<T> {

    private final static String name = "kafka-ingestor-client";

    public ClientUrlTarget(final Class<T> type, final String url) {
        super(type, name, url);
    }

    public ClientUrlTarget(final Class<T> type, final String name, final String url) {
        super(type, name, url);
    }
}