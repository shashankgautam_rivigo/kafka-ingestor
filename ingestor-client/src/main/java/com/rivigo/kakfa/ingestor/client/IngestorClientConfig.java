package com.rivigo.kakfa.ingestor.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author shashankgautam
 */
@Data
@Builder
@AllArgsConstructor
public class IngestorClientConfig {

    public enum LogLevel {
        NONE, BASIC, HEADERS, FULL
    }

    private String url = "http://localhost:18000";
    private int connectTimeoutMillis = 10;
    private int readTimeoutMillis = 60;
    private LogLevel logLevel = LogLevel.BASIC;
}