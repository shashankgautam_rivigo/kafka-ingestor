package com.rivigo.kakfa.ingestor.client.feign;

import com.rivigo.kakfa.ingestor.client.util.Serializer;
import feign.RequestTemplate;
import feign.codec.EncodeException;
import feign.codec.Encoder;

import java.lang.reflect.Type;

/**
 * @author shashankgautam
 */
public class ClientEncoder implements Encoder {

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON = "application/json";

    @Override
    public void encode(final Object object, final Type bodyType, final RequestTemplate template) throws EncodeException {
        if (object != null) {
            final String body = Serializer.INSTANCE.serialize(object);
            template.body(body);
            template.header(CONTENT_TYPE, APPLICATION_JSON);
        }
    }
}