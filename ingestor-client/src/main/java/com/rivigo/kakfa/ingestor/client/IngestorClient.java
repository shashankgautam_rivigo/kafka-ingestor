package com.rivigo.kakfa.ingestor.client;

import com.rivigo.kakfa.ingestor.client.feign.ClientEncoder;
import com.rivigo.kakfa.ingestor.client.feign.IngestorFeignClient;
import com.rivigo.kakfa.ingestor.client.feign.ClientUrlTarget;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.jaxrs.JAXRSContract;
import feign.okhttp.OkHttpClient;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shashankgautam
 */
@Slf4j
public class IngestorClient {

    private final IngestorClientConfig clientConfig;

    @Getter
    private Ingestor ingestor;

    /**
     * @param clientConfig ::
     */
    public IngestorClient(final IngestorClientConfig clientConfig) {
        this.clientConfig = clientConfig;
        this.ingestor = new Ingestor(builder().target(new ClientUrlTarget<>(IngestorFeignClient.class, clientConfig.getUrl())));
        log.info("Initialized Ingestor client...");

    }

    private Feign.Builder builder() {
        return Feign.builder()
                .logger(new feign.Logger.JavaLogger())
                .logLevel(getLogLevel(clientConfig))
                .options(new Request.Options(clientConfig.getConnectTimeoutMillis(), clientConfig.getReadTimeoutMillis()))
                .client(new OkHttpClient())
                .contract(new JAXRSContract())
                .encoder(new ClientEncoder());
    }

    private Logger.Level getLogLevel(final IngestorClientConfig clientConfig) {
        switch (clientConfig.getLogLevel()) {
            case BASIC:
                return Logger.Level.BASIC;
            case FULL:
                return Logger.Level.FULL;
            case NONE:
                return Logger.Level.NONE;
            case HEADERS:
                return Logger.Level.HEADERS;
            default:
                return Logger.Level.FULL;
        }
    }
}
