package com.rivigo.kakfa.ingestor.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rivigo.kafka.ingestor.common.Event;
import feign.Response;

import java.io.IOException;

/**
 * @author shashankgautam
 */
public class Example {

    public static void main(String[] args) throws IOException {
        // Api Config common to both strategy - url or discovery
        final IngestorClientConfig config = IngestorClientConfig.builder()
                .connectTimeoutMillis(10)
                .readTimeoutMillis(60)
                .logLevel(IngestorClientConfig.LogLevel.FULL)
                .url("http://localhost:18000")
                .build();
        final IngestorClient client = new IngestorClient(config);
        for (int count = 1; count <= 10000; count++) {
            final Response response = client.getIngestor().ingest(getEvent());
            System.out.println(response.status());
            System.out.println(response.body());
        }
    }

    static Event getEvent() throws IOException {
        return new ObjectMapper().readValue(eventStr.getBytes(), Event.class);
    }

    static String eventStr = "{\n" +
            "\t\"metadata\": {\n" +
            "\t\t\"tenant\": \"zoom\",\n" +
            "\t\t\"sessionId\": \"1234\",\n" +
            "\t\t\"timestamp\": 1512229627653,\n" +
            "\t\t\"eventId\": 123,\n" +
            "\t\t\"eventName\": \"test\",\n" +
            "\t\t\"schemaName\": \"test-schema\",\n" +
            "\t\t\"schemaVersion\": 1\n" +
            "\t},\n" +
            "\t\"data\": \"Now its your turn to play with the big array, in this tutorial, I just used very small number of data, but actually the big \"\n" +
            "}";
}
