package com.rivigo.kakfa.ingestor.client.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rivigo.kakfa.ingestor.client.feign.ClientException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shashankgautam
 */
@Slf4j
public class Serializer {

    public static final Serializer INSTANCE = new Serializer();

    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * Constructor
     */
    private Serializer() {
    }

    /**
     * Serialize content to string
     *
     * @param content to serialize
     * @return string
     */
    public String serialize(final Object content) {
        try {
            return mapper.writeValueAsString(content);
        } catch (final JsonProcessingException e) {
            log.error("Unable to serialize object. Error - ", e);
            throw new ClientException("Unable to serialize object");
        }
    }
}
