package com.rivigo.kafka.ingestor.common;

import lombok.*;

/**
 * @author shashankgautam
 */
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
public class Event {
    private Metadata metadata;
    private Object data;
}
