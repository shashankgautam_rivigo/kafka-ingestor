package com.rivigo.kafka.ingestor.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * @author shashankgautam
 */
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Metadata {
    private String tenant;
    private String sessionId;
    private Timestamp timestamp;
    private long eventId;
    private String eventName;
    private String schemaName;
    private int schemaVersion;
}
